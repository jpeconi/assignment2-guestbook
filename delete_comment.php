<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This page is a simple script for deleting a comment. IF the user decides
 * to delete a comment this script is called upon.
 * ***********************************************************************/
include "redirect.php";
if(isset($_POST['delete'])) {
    // Grabs the id of the comment being edited
    $id = $_POST['comment_id'];
    // Build the query to update the comment being edited
    $deleteQuery = "DELETE FROM comments WHERE id={$id}";
    // Fire it off
    $conn->query($deleteQuery);
    $commentMessage = "Comment deleted";
}
?>