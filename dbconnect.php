<?php
/**********************************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:12 PM
 * Internet Programming II - Guest book Login
 *
 * This is the script that handles the connection to the database. I have setup
 * a user account for the guest book admin. Also sends an error message to the
 * screen if a connection cant be made.
 *
 ***********************************************************************************/
include "redirect.php";
// The constants which will contain the login info
define('DBHOST', 'localhost');
define('DBUSER', 'guestbook_admin');
define('DBPASS', 'donnie');
define('DBNAME', 'guestbook');
// Setup a connection object
$conn = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
// Setup an error message if the connection cant be made and die!
if ( !$conn ) {
    die("Connection failed : " . $conn->connect_errno);
}
