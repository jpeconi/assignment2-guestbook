<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This is the login page for the guest book web app. This page provides
 * the user with a form to enter their login information. This page
 * submits to a PHP script which will handle all the validation. This page
 * also allows the user to select the remember me option which allow them
 * to remain logged in.
 *
 * Styles for this page are all done using bootstrap
 ************************************************************************/
session_start();
// Connection to the database
include("dbconnect.php");
// Initialize error message
$errorMessage = "";
// Check to see if session variable is set and redirect them to the comments page if so
if (isset($_SESSION['username'])) {
    header("location: index.php");
}
include "check_cookie.php";
// Include file containing the validation for the login
include "validate_login.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <!-- Stylesheets -->
    <link href="css/main.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css"
          rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <!-- The login box -->
    <div class="card card-container">
        <p id="profile-name" class="profile-name-card">Comment <span class="red">Zone</span></p>
        <form class="form-signin" action="login.php" method="post">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="userName" class="form-control" placeholder="username" name="username" required>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" name="remember" value="true"> Remember me
                </label>
            </div>
            <input class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="Sign In">
        </form>
        <a href="register.php" class="register-link">Register</a>
        <p class="red"><?php echo $errorMessage ?></p>
        <a href="index.php" class="register-link">Continue as guest</a>
    </div>
</div>
</body>
</html>
