<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This page will handle the register functionality for the application.
 * This page will allow the users to enter information and sign up for
 * an account. It will check to see if the user already exists, if they
 * don't they will be entered into the database.
 ************************************************************************/
//Start the session
session_start();
// Including the page containing the logic to validate the register
include "validate_register.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <!-- Stylesheets -->
    <link href="css/register.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css"
          rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript"></script>
</head>
<body>
    <div class="container-fluid" id="regContainer">
        <?php
        // Little message telling the user they have successfully created an account
        // They are being redirected to the login screen
            if($isSuccess) {
            ?>
            <div class="row" id="message">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p>Account created successfully redirecting to login page</p>
                </div>
            </div>
        <?php
            // Display the form if they haven't registered
            } else {
        ?>
        <div class="row">
            <!-- The registration form -->
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="POST" action="register.php" role="form" enctype="multipart/form-data">
                        <div class="form-group">
                            <h2>Create account</h2>
                        </div>
                        <!-- Username -->
                        <div class="form-group">
                            <label class="control-label" for="username">Username</label>
                            <input id="username" name="username" type="text" maxlength="50" class="form-control" required>
                        </div>
                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input id="email" name="email" type="email" maxlength="50" class="form-control" required>
                        </div>
                        <!-- Password -->
                        <div class="form-group">
                            <label class="control-label" for="password">Password</label>
                            <input id="password" name="password" type="password" maxlength="25" class="form-control"
                                   placeholder="at least 8 characters" required>
                        </div>
                        <!-- Confirm password -->
                        <div class="form-group">
                            <label class="control-label" for="confPass">Password again</label>
                            <input id="confPass" name="confPass" type="password" maxlength="25" class="form-control">
                        </div>
                        <!-- Profile Image Upload -->
                        <div class="form-group">
                            <label class="control-label" for="file">Profile Img</label>
                            <input type="file" name="profileImg">
                        </div>
                        <!-- Error Message -->
                        <div class="form-group">
                            <p class="red"><?php echo $errorMessage ?></p> <!-- Echo the error message -->
                        </div>
                        <!-- Submit Button -->
                        <div class="form-group">
                            <input id="signupSubmit" name='submit' type="submit" class="btn btn-info btn-block"
                                   value="Create your account">
                        </div>
                        <hr>
                        <!-- Google Re-Captcha -->
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Lf7tQgUAAAAAGLDrtjWWrfoMe8n3HZxYia5uqlV"></div>
                        </div>
                        <!-- Button to login -->
                        <p>Already have an account?<a id="signIn" href="login.php"> Sign in</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
        }
    ?>
</body>
</html>
