<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This page contains the script to retrieve the comments from the db
 * for the main comment page. This page contains all the logic to display
 * as well as give the message to the user that there is nothing to display
 * in the even that the comment database is empty.
 ************************************************************************/
include "redirect.php";
// Build the query to grab the comments from the database. This script also
// had to deal with grabbing the filename for the user image so that the
// right image would be displayed next to each post.
$query = "SELECT title,comments.userName,comment,time,img,id FROM comments,users WHERE users.userName = comments.userName";
$result = $conn->query($query);
$numRows = $result->num_rows;
if ($numRows == 0) {
    ?>
    <div class="row col-md-6 col-md-offset-3">
        <h3>Oops there is nothing to display</h3>
    </div>
    <?php
} else {
    // We found results, loop through and build each comment using html
    // Each comment will look the same. Displays the username, as well as
    // the date entered. Also displays the image of the user that posted.
    while ($row = $result->fetch_assoc()) {
        ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-4">
                    <h3><em><strong>#<?php echo $row['title']; ?></strong></em></h3>
                </div>
            </div>
            <div class="row eachComment">
                <div class="col-md-1 col-md-offset-3">
                    <div class="commentThumb">
                        <img class="img-responsive user-photo commentThumbnail"
                             src="img/<?php echo $row['img']; ?>">
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="panel panel-default">
                        <form method="post" action="index.php">
                        <div class="panel-heading">
                            <span><strong><?php echo $row['userName']; ?></strong></span>
                                <span class="text-muted">commented <?php echo $row['time']; ?></span>
                            <?php
                            // Check to see if the logged in user posted the comment
                            // IF they did give them an edit option
                            if(isset($_SESSION['username'])) {
                                if ($row['userName'] == $_SESSION['username']) {
                                    ?>
                                <input type="hidden" name="comment_id" value="<?php echo $row['id']?>">
                                <input type="submit" name="edit" value="edit" class="editButton btn btn-success">
                                <input type="submit" name="delete" value="delete" class="deleteButton btn btn-danger">
                            <?php
                                }
                            }
                            ?>
                        </form>
                        </div>
                        <div class="panel-body">
                            <?php echo $row['comment']; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }
}
?>
