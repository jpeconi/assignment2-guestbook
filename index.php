<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This will be the main page. This will handle all the comments. Provide
 * the user with a spot to enter a comment to the guest book. Will also
 * have the ability to edit their own posts as well as delete them.
 *
 * The logo for the page I borrowed from a google image search.
 * www.logoarena.com
 *
 * I added a test user ----- username : jay ---- password: jay ----------
 ************************************************************************/
session_start();
// The database connection
include "dbconnect.php";
include_once "check_cookie.php";
// Easy variable for logged in status
if (isset($_SESSION['username'])) {
    $isLoggedIn = true;
} else {
    $isLoggedIn = false;
}
// Default POST variable which will help the page either edit or submit
// Added this so in the form it will add a new comment by default.
// However, if they decide to edit a comment this will change.
// Had to add this workaround to use the one page as oppose to different pages.
$methodValue = "submit";
// Default the comment message
$commentMessage = "";
// Default the comment id to null
$id = null;
// Default Title
$subject = "Subject";
// Default the comment message
$commentMessage = "";
// If the edit comment is set. Grab the details from the comment to edit
if (isset($_POST['edit'])) {
    // The query to retrieve the comment they are editing
    $commentID = $_POST['comment_id'];
    $query = "SELECT * FROM comments WHERE id={$commentID}";
    $result = $conn->query($query);
    $row = $result->fetch_row();
    $subject = $row[2];
    // Get the comment from the array returned
    $comment = $row[3];
    // Get the comment ID from the returned row
    // Also overwrite the ID variable
    $id = $row[0];
    // Change the post value to edit ( Used in the form below )
    // This will allow me to determine whether to edit or submit
    // a new comment
    $methodValue = "editOption";
}
// Heading information for display
if ($isLoggedIn) {
    $userName = $_SESSION['username'];
    $userImage = $_SESSION['userImg'];
    $prompt = "Logged in as";
} else {
// Defaults for the heading information
    $userImage = "default.png";
    $userName = "Guest";
    $prompt = "<a href='login.php'>Login</a>";
}


include "delete_comment.php";
include "submit_comment.php";
// Default message
$comment = "Say something";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <!-- Stylesheets -->
    <link href="css/main.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css"
          rel="stylesheet">
    <!-- The WYSIWYG editor library -->
    <script src="js/ckeditor/ckeditor.js" type="text/javascript"></script>
</head>
<body>
    <div class="container-fluid">
        <!-- The header at the top of the page -->
        <div class="row">
            <img src="img/<?php echo $userImage; ?>" class="thumbnail">
            <p id="loggedInMessage"><?php echo $prompt;?></p>
            <br>
            <p id="userDisplay"><?php echo $userName; ?></p>
            <img src="img/logo.jpg" class="logo">
            <?php
            // If the user is logged in show a logout button
                if($isLoggedIn) {
                    ?>
                    <a id="logout" href="logout.php">logout</a>
                    <?php
                }
            ?>
        </div>
        <hr>
        <?php
        // If the user is logged in
        if ($isLoggedIn) {
        ?>
        <!-- The comment Posting part only displayed if the user is logged in -->
        <div class="container col-lg-8 col-lg-offset-2 text-center">
            <div class="row">
                <div class="col-md-12">
                    <p class="red"><?php echo $commentMessage; ?></p>
                </div>
            </div>
            <form class="comment-form" action="index.php" method="post">
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                        <label for="commentTitle">Comment Title</label>
                        <input type="text" class="form-group" id="commentTitle" name="commentTitle"
                               value="<?php echo $subject; ?>">
                    </div>
                </div>
                <div class="row">
                    <div id="commentBox">
                        <div id="topBoxComment">
                            <p class="left white">Leave a Comment</p>
                        </div>
                        <input type="submit" value="submit" name="<?php echo $methodValue ?>" class="btn btn-danger subButton">
                        <!--Hidden field which will send the id through. Will send NULL and have no effect unless the edit comment is set-->
                        <input type="hidden" value="<?php echo $id ?>" name="id">
                        <textarea class="form-group" id="commentArea" name="commentArea"><?php echo $comment; ?></textarea>
            </form>
        </div>
    </div>
    </div>
    <!-- Pull the comments from the database -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="commentHeadline">User Comments</h3>
            <hr class="commentDivider">
        </div><!-- /col-sm-12 -->
    </div><!-- /row -->
    <?php
        }
        include "get_comments.php";
    ?>
</body>
<script type="text/javascript">
    window.onload = function () {
        CKEDITOR.replace('commentArea');
    };
</script>
</html>

