<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This is a simple little logout script. This page handles the logout
 * process for the guestbook. This simple few lines just unsets the
 * session variable and deletes the cookie. This forces the user back to
 * the login page.
 ************************************************************************/
include "redirect.php";
session_start();
// Unset the Session variable
unset($_SESSION['username']);
// Unset the cookie for the logged in user
setcookie("userName","",time() - 10000000);
setcookie("password","",time() - 10000000);
// send us back to login
header("location: index.php");
?>