<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This page is the login validation. This contains the logic behind
 * checking to see if the credentials are good. This page makes a call
 * to the database and checks to see if the username matches up with
 * the password that is provided by the user.
 ************************************************************************/
include "redirect.php";
// Check to see if the username and password are filled out
if(isset($_POST['username']) && isset($_POST['password'])) {
//Strip any dangerous characters to avoid sql injection
    $userName = $conn->real_escape_string($_POST['username']);
    $password = $conn->real_escape_string($_POST['password']);
// Build the query to fire off to the database
    // This also hashes the password using the built in sha1 function
    $query = "SELECT count(*) FROM users WHERE userName='".$userName."' and userPass='".sha1($password)."'";
    // The result of the query
    $result = $conn->query($query);
    // Get the row from the result
    $row = $result ->fetch_row();

// Simply check to see if the row is equal to 1
    // No need to check anything else here as the registration page handles all the
    // worries of multiple people having the same account details
    // If its equal to 1 it means that there is a match for the username and pass in the database
    if($row[0] == 1) {
        $imageQuery = "SELECT img FROM users WHERE userName='".$userName."'";
        $imageResult = $conn->query($imageQuery);
        $row = $imageResult->fetch_row();

        // set the session username variable
        $_SESSION['username'] = $userName;
        $_SESSION['userImg'] = $row[0];
        // set the cookie if they selected the remember me option
        if(isset($_POST['remember']) && $_POST['remember'] == "true") {
            setcookie("userName",$userName,time() + 10000000);
            setcookie("userImg",$row[0],time() + 10000000);
            setcookie('password',sha1($password),time() + 10000000);
        }
        // redirect to the main page
        header("location: index.php");
    } else {
        $errorMessage = "Invalid Credentials";
    }

}
?>