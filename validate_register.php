<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This is the more complex logic in the app. This page handles all the
 * validation required to create an account. It also sanitizes data and
 * provides a captcha system to make sure the database cant be filled
 * with bad accounts made by bots.
 ************************************************************************/
include "redirect.php";
// Array to hold the names of all the fields from the form page
$fieldsRequired = array('username', 'email', 'password', 'confPass');
// Global variables I will use throughout the functions
$isValid = true;
$errorMessage = "";
$isSuccess = false;
$fileType = "";
$userImg = "";
// Connection to the database
include("dbconnect.php");
// Check to see if anything has been entered for POST data
// Page also has client side validation. However, in the event that is
// manipulated, this will handle it as well
    if (!empty($_POST)) {
// Check to make sure all fields are filled in
        // Loop through and check them all for empty
        foreach ($fieldsRequired as $field) {
            if (empty($_POST[$field])) {
                $errorMessage = "All fields required";
                $isValid = false;
                echo $field;
            }
        }
// If there are no empty fields proceed here
        if ($isValid) {
            // Sanitize all the data coming from the user
            $email = $conn->real_escape_string(trim($_POST['email']));
            $user = $conn->real_escape_string(trim($_POST['username']));
            $password = $conn->real_escape_string(trim($_POST['password']));
            $confPass = $conn->real_escape_string(trim($_POST['confPass']));
            // Check the email first to see if its already in the database
            if (verifyEmail()) {
                // Check the username
                if (verifyUserName()) {
                    // Check to see the passwords match
                    if (verifyPasswords()) {
                        // The Captcha provided by Google - I used the API Docs to get this working
                        if (verifyCaptcha()) {
                            if (processProfileImg()) {
                                // Create the user, everything checked out
                                createUser();
                            }
                        }
                    }
                }
            }
        }
    }

/**
 * @return bool
 * @author Jamison Peconi
 * The purpose of this function is to verify the email address.
 * It makes a call to the database and determines whether or not a user with
 * that email address has created an account before. Return true or false
 * depending on whether or not it checks out.
 */
function verifyEmail()
{
    // Globals
    global $conn;
    global $errorMessage;
    global $email;
    // Check to see if email is already registered
    $emailQuery = "SELECT count(*) FROM users WHERE userEmail='" . $email . "'";
    $emailResult = $conn->query($emailQuery);
    $row = $emailResult->fetch_row();
    // Check to see if the row is populated or not
    if ($row[0] == 1) {
        $errorMessage = "That email has already registered";
        return false;
    } else {
        return true;
    }
}

/**
 * @return bool
 * @author Jamison Peconi
 * The purpose of this function is to verify the user name.
 * It makes a call to the database and determines whether or not a user with
 * that user name has created an account before. Return true or false
 * depending on whether or not it checks out.
 */
function verifyUserName()
{
    // Globals
    global $conn;
    global $errorMessage;
    global $user;
    // Query to see if the user name already exists
    $query = "SELECT * FROM users WHERE userName='" . $user . "'";
    $userResult = $conn->query($query);
    $row = $userResult->fetch_row();
    // Check to see if it returns a result or not
    if ($row[0] > 0) {
        $errorMessage = "Username already exists";
        return false;
    } else {
        return true;
    }
}

/**
 * @return bool
 * @author Jamison Peconi
 * The purpose of this function is to verify that both passwords that
 * were entered by the user match. Return true or false based on the
 * outcome.
 */
function verifyPasswords()
{
    // Global variables
    global $password;
    global $confPass;
    global $errorMessage;
    // Check the passwords matching
    if ($password != $confPass) {
        $errorMessage = "Passwords do not match";
        return false;
    } else {
        return true;
    }
}

/**
 * @return bool
 * @author Jamison Peconi
 * The purpose of this function is to create the user. If all the validating
 * checks out the user is then created. This function makes a call to the
 * database and inserts the information provided by the user.
 */
function createUser()
{
    // Global variables I will be using
    global $conn;
    global $user, $password, $email;
    global $errorMessage;
    global $isSuccess;
    global $userImg;
    // The query to insert into the database
    $createQuery = "INSERT INTO users VALUES (NULL,'" . $user . "','" . $email . "','" . sha1($password) . "','" . $userImg . "')";
    if ($conn->query($createQuery)) {
        header("refresh:3;url = login.php");
        $isSuccess = true;
    } else {
        $errorMessage = "There was a problem creating your account!";
    }

}

/**
 * @return bool
 * @author Jamison Peconi
 * The purpose of this function is to verify the captcha. This function
 * uses an API provided by Google. I followed the documentation to figure out
 * how to use this. It sends out a request to the google site and returns a
 * JSON object. You just have to grab whether it was a success or not and
 * handle what happens accordingly.
 */
function verifyCaptcha()
{
    // The global error message
    global $errorMessage;
    // The secret key provided from Google for use with the API
    $secret = "6Lf7tQgUAAAAAElD64yH1_QSyhnUKcBfUczwyFN1";
    // The POST response from the form
    $response = $_POST["g-recaptcha-response"];
    // Store the JSON returned from the API call
    $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
    // Decode the JSON to an object
    $captcha_success = json_decode($verify);

    // Check the success attribute to determine whether its a pass or fail
    if ($captcha_success->success == false) {
        $errorMessage = "You failed the captcha";
        return false;
    }
    return true;

}

/**
 *
 * @author Jamison Peconi
 * This function handles the upload functionality of the profile image. This function
 * takes the file uploaded by the user and names it according to the username. It then stores
 * the image in the img directory. It also stores a reference to the image in a column in the user
 * entry to the database. If the user does not specify a profile image, then a default will be provided.
 * This function also changes the permission in a UNIX environment so the filename has the correct
 * permissions. Checks are done to be sure the img too big and makes sure its an image of type
 * gif, png, or jpg / jpeg.
 */
function processProfileImg()
{
    // Global variables
    global $user;
    global $errorMessage;
    global $userImg;
    // Get the current working directory
    $root = getcwd();
    // Set the directory to store the images
    $targetDir = $root . "/img/";
    // The path of the image to be saved
    $targetFile = $targetDir . basename($_FILES['profileImg']['name']);
    // The file extension or type
    $fileType = pathinfo($targetFile, PATHINFO_EXTENSION);
    // Get the image size
    $imageSize = $_FILES['profileImg']['size'];
    // Array of the allowed types to upload
    $allowedTypes = array('jpeg', 'jpg', 'gif', 'png');
    $isValid = false;

    // If they did not upload an image set their image to the default
    if ($_FILES['profileImg']['name'] == "" && $_FILES['profileImg']['size'] == 0) {
        $userImg = "default.png";
        return true;
    } else {
        // Check to see if the image is bigger than 300k
        if ($imageSize > 300000) {
            $errorMessage = "File is too large. 300k max";
            return false;
        } else {
            // Loop through the allowed types and compare to the file type
            // If it is an allowed type turn the flag to true
            foreach ($allowedTypes as $type) {
                if ($fileType == $type) {
                    $isValid = true;
                }
            }
            // If its under 300k and an accepted filetype
            if ($isValid) {
                // Set the path to be written
                $imagePath = $targetDir . $user . "." . $fileType;
                // Move the file from the temp php directory to the img folder
                // Had to change the file permissions on the MAC
                // Have yet to test on Windows
                move_uploaded_file($_FILES['profileImg']['tmp_name'], $imagePath);
                // Change the file permissions so its not locked after being uploaded
                chmod($imagePath, 0777);
                // Variable to be used in the sql query in the create user function
                $userImg = $user . "." . $fileType;
            } else {
                $errorMessage = "Incorrect filetype. Must be jpg/jpeg, gif, png";
                return false;
            }
            return true;
        }
    }
}

