<?php
/**********************************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:12 PM
 * Internet Programming II - Guest book Login
 *
 * Added this file to ensure that if the users decide to go to any of the pages
 * by typing them into the browser window without accessing them the proper way
 * through the pages.
 *
 ***********************************************************************************/

if(!isset($_SESSION)) {
    header("location: index.php");
}