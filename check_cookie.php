<?php
/**
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-14
 * Time: 1:56 PM
 * Internet Programming II - Assignment guest book
 */
include "redirect.php";
// Check to see if cookie exists
// if it exists check to see if the password stored in the cookie
// matches the password for that same user in the database. If it
// doesn't then run the page like normal, and they will have to log back in.
// This would mean that someone tampered with the username cookie.
// If the password matches, then the user will be sent straight to the comments page
// and be logged in. Storing the username in the session.
if (isset($_COOKIE['userName']) && !isset($_SESSION['username'])) {
    $query = "SELECT userPass FROM users WHERE userName='{$_COOKIE['userName']}'";
    $result = $conn->query($query);
    $row = $result->fetch_row();
    if ($_COOKIE['password'] == $row[0]) {
        $_SESSION['username'] = $_COOKIE['userName'];
        $_SESSION['userImg'] = $_COOKIE['userImg'];
        header("location: index.php");

    }
}
