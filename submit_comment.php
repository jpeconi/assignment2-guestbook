<?php
/***********************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-10-06
 * Time: 9:15 PM
 * Internet Programming II
 * Assignment 2 - Login Comments Page
 *
 * This page contains the script to submit a comment to the database.
 * This is also for editing a comment. This script will check to ensure that
 * the user entered a comment. Will not let you post a blank comment.
 * If the user has left no fields blank it will determine whether to add
 * it as a new comment, or edit an existing comment depending on what
 * the user is doing.
 ************************************************************************/
include "redirect.php";
// Grab the session username and store it locally
if(isset($_SESSION['username'])) {
    $user = $_SESSION['username'];
}
// Checks to see the POST has been set, in case the user lands on this page
// without submitting the form.
if (isset($_POST['commentArea']) && isset($_POST['commentTitle'])) {
    // Grab the comment
    $comment = $_POST['commentArea'];
    // Grab comment title
    $title = $_POST['commentTitle'];
    // Check to see its not blank
    if ($comment != "" && $title != "") {
        if(!isset($_POST['editOption'])) {
            // Call the function to submit
            submitComment();
            // Confirmation message
            $commentMessage = "Comment added";
            // if they were editing. Then run the edit comment script instead
        } else {
            editComment();
            $commentMessage = "Comment edited";
        }
        // Unset the POST and local variable. This will ensure that
        // if the page is refreshed it wont submit the same comment again.
        // Also resets the default comment message in the comment editor.
        unset($_POST['commentArea']);
    } else {
        // Set an error message
        $commentMessage = "Comment or title cannot be blank";
    }
}

/**
 * @author Jamison Peconi
 * @since 10/11/2016
 * Internet Programming II
 * Guest book - Web App
 *
 * This function is responsible for the logic and the query to submit the
 * comment to the database. Fairly simple query, that will only fire as
 * long as the criteria has been met above.
 */
function submitComment()
{
    // Some global variables to be used
    global $conn;
    global $comment;
    global $user;
    global $title;
    // Sanitize the fields before entering
    $comment = $conn->real_escape_string($comment);
    $user = $conn->real_escape_string($user);
    $title = $conn->real_escape_string($title);
    // Build the query
    $query = "INSERT INTO comments VALUES (NULL,'$user','$title','$comment',now())";
    // Run it
    $conn->query($query);
}

/**
 * @author Jamison Peconi
 * @since 10/11/2016
 * Internet Programming II
 * Guest book - Web App
 *
 * This function is responsible for the logic and the query to edit a
 * comment in the database. Fairly simple query, that will only fire as
 * long as the user has filled in the fields properly.
 */
function editComment() {
    // Grab the connection
    global $conn;
    // Sanitize the data
    $id = $conn->real_escape_string($_POST['id']);
    $comment = $conn->real_escape_string($_POST['commentArea']);
    $title = $conn->real_escape_string($_POST['commentTitle']);
    // Build the query to update the comment being edited
    $editQuery = "UPDATE comments SET time=now(),title='{$title}',comment='{$comment}' WHERE id={$id}";
    // Fire it off
    $conn->query($editQuery);
}